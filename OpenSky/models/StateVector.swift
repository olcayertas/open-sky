//
// Created by Olcay Ertaş on 11.08.2020.
// Copyright (c) 2020 Olcay Ertaş. All rights reserved.
//

import Foundation

struct StateVector: Codable, Hashable {

    var icao24: String?
    var callsign: String?
    var origin_country: String?
    var time_position: Int?
    var last_contact: Int?
    var longitude: Double?
    var latitude: Double?
    var baro_altitude: Double?
    var on_ground: Bool?
    var velocity: Double?
    var true_track: Double?
    var vertical_rate: Double?
    var sensors: [Int]?
    var geo_altitude: Double?
    var squawk: String?
    var spi: Bool?
    var position_source: Int?

    private static let selfPropertyCount = 17

    init(data: [Any]) {
        guard data.count == StateVector.selfPropertyCount else {
            return
        }
        if let icao24 = data[0] as? String {
            self.icao24 = icao24
        }
        if let callsign = data[1] as? String {
            self.callsign = callsign
        }
        if let origin_country = data[2] as? String {
            self.origin_country = origin_country
        }
        if let time_position = data[3] as? Int {
            self.time_position = time_position
        }
        if let last_contact = data[4] as? Int {
            self.last_contact = last_contact
        }
        if let longitude = data[5] as? Double {
            self.longitude = longitude
        }
        if let latitude = data[6] as? Double {
            self.latitude = latitude
        }
        if let baro_altitude = data[7] as? Double {
            self.baro_altitude = baro_altitude
        }
        if let on_ground = data[8] as? Bool {
            self.on_ground = on_ground
        }
        if let velocity = data[9] as? Double {
            self.velocity = velocity
        }
        if let true_track = data[10] as? Double {
            self.true_track = true_track
        }
        if let vertical_rate = data[11] as? Double {
            self.vertical_rate = vertical_rate
        }
        if let sensors = data[12] as? [Int] {
            self.sensors = sensors
        }
        if let geo_altitude = data[13] as? Double {
            self.geo_altitude = geo_altitude
        }
        if let squawk = data[14] as? String {
            self.squawk = squawk
        }
        if let spi = data[15] as? Bool {
            self.spi = spi
        }
        if let position_source = data[16] as? Int {
            self.position_source = position_source
        }
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(icao24)
    }

    static func ==(lhs: StateVector, rhs: StateVector) -> Bool {
        lhs.icao24 == rhs.icao24
    }
}


