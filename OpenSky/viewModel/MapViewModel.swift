//
// Created by Olcay Ertaş on 11.08.2020.
// Copyright (c) 2020 Olcay Ertaş. All rights reserved.
//

import Foundation
import Combine
import os.log
import MapKit


class MapViewModel: ObservableObject {

    @Published var vectors: [StateVector] = [] {
        willSet {
            self.objectWillChange.send()
        }
    }

    @Published var isRefreshing: Bool = false {
        willSet {
            self.objectWillChange.send()
        }
    }

    private let apiClient = StateVectorApiClient()

    private var edges: Edges?
    private var completion: (() -> Void)?
    private var timer: Timer?
    private var addedVectors: [StateVector] = []
    private var cancellable: AnyCancellable?
    private var removedVectors: [StateVector] = []

    var addedAnnotations: [MKAnnotation] {
        var annotations: [MKAnnotation] = []
        addedVectors.forEach { vector in
            let annotation = MKPointAnnotation()
            annotation.title = vector.callsign
            annotation.subtitle = vector.icao24
            annotation.coordinate = CLLocationCoordinate2D(
                    latitude: CLLocationDegrees(vector.latitude ?? 0),
                    longitude: CLLocationDegrees(vector.longitude ?? 0)
            )
            annotations.append(annotation)
        }
        return annotations
    }


    deinit {
        cancellable?.cancel()
    }

    private func startTimer() {
        timer = Timer.scheduledTimer(
                timeInterval: 5,
                target: self,
                selector: #selector(getPlanesOnTick),
                userInfo: nil, repeats: true
        )
    }

    private func stopTimer() {
        timer?.invalidate()
    }

    @objc func getPlanesOnTick() {
        if let edges = edges {
            getPlanes(edges: edges, completion: completion)
        }
    }

    func getPlanes(edges: Edges, completion: (() -> Void)? = nil) {
        self.edges = edges
        self.completion = completion
        getPlanes(
                lamin: edges.sw.latitude,
                lamax: edges.ne.latitude,
                lomin: edges.sw.longitude,
                lomax: edges.ne.longitude
        ) {
            completion?()
        }
    }

    func getPlanes(
            lamin: Double,
            lamax: Double,
            lomin: Double,
            lomax: Double,
            completion: (() -> Void)? = nil
    ) {
        cancellable?.cancel()
        timer?.invalidate()
        isRefreshing = true
        cancellable = apiClient
                .getResults(lamin: lamin, lamax: lamax, lomin: lomin, lomax: lomax)
                .sink(receiveCompletion: { [weak self] completion in
                    self?.onCompletion(completion)
                    self?.isRefreshing = false
                }, receiveValue: { vectors in
                    self.onReceive(vectors: vectors)
                    completion?()
                    self.startTimer()
                })
    }

    private func onReceive(vectors: [StateVector]) {
        let oldSet = Set(self.vectors)
        self.addedVectors = []
        vectors.forEach { vector in
            if oldSet.contains(vector) == false {
                self.addedVectors.append(vector)
            }
        }
        let newSet = Set(vectors)
        self.removedVectors = []
        self.vectors.forEach { vector in
            if newSet.contains(vector) == false {
                self.removedVectors.append(vector)
            }
        }
        self.vectors = vectors
        os_log("Result count %{public}@", log: .mapViewModel, type: .info, "\(vectors.count)")
    }

    private func onCompletion(_ completion: Subscribers.Completion<Error>) {
        switch completion {
        case .finished:
            os_log("Completed", log: .mapViewModel, type: .info)
        case .failure(let error):
            os_log("Error %{public}@", log: .mapViewModel, type: .error, error.localizedDescription)
        }
    }

    public func getAnnotationsToRemove(from annotations: [MKAnnotation]) -> [MKAnnotation] {
        let removedSet: Set<StateVector> = Set(self.removedVectors)
        var annotationsToRemove: [MKAnnotation] = []
        annotations.forEach { annotation in
            let contains = removedSet.contains { (vector: StateVector) -> Bool in
                vector.icao24 == annotation.subtitle
            }
            if contains {
                annotationsToRemove.append(annotation)
            }
        }
        return annotationsToRemove
    }
}
