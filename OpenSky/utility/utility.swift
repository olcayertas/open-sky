//
// Created by Olcay Ertaş on 18.07.2020.
// Copyright (c) 2020 Olcay Ertaş. All rights reserved.
//

import Foundation
import UIKit
import os.log
import SwiftUI
import Combine

struct KeyboardAdaptive: ViewModifier {

    @State private var keyboardHeight: CGFloat = 0

    func body(content: Content) -> some View {
        content.padding(.bottom, keyboardHeight).onReceive(Publishers.keyboardHeight) {
            self.keyboardHeight = $0
        }
    }
}
