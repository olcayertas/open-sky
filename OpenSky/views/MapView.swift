//
// Created by Olcay Ertaş on 11.08.2020.
// Copyright (c) 2020 Olcay Ertaş. All rights reserved.
//

import Foundation
import SwiftUI
import MapKit
import Combine
import os.log


struct MapView: UIViewRepresentable {

    public let viewModel = MapViewModel()
    private let mkMapView = MKMapView()

    class Coordinator: NSObject, MKMapViewDelegate {

        var parent: MapView
        private let identifier = "Annotation"

        init(_ parent: MapView) {
            self.parent = parent
            super.init()
        }

        func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
            parent.getPlanes(edges: mapView.edgePoints())
        }

        func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
            guard annotation is MKPointAnnotation else { return nil }
            var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
            if annotationView == nil {
                annotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: identifier)
                annotationView!.canShowCallout = true
                annotationView!.clusteringIdentifier = "cluster"
            } else {
                annotationView!.clusteringIdentifier = "cluster"
                annotationView!.displayPriority = .defaultHigh
                annotationView!.annotation = annotation
            }
            return annotationView
        }
    }

    func getPlanes(edges: Edges) {
        viewModel.getPlanes(edges: edges) { [self] in
            self.removeAnnotations()
            self.mkMapView.addAnnotations(self.viewModel.addedAnnotations)
        }
    }

    private func removeAnnotations() {
        let annotationsToRemove = self.viewModel.getAnnotationsToRemove(from: self.mkMapView.annotations)
        self.mkMapView.removeAnnotations(annotationsToRemove)
    }

    func makeCoordinator() -> Coordinator {
        Coordinator(self)
    }

    func makeUIView(context: Context) -> MKMapView {
        mkMapView.delegate = context.coordinator
        mkMapView.showsScale = true
        mkMapView.showsCompass = true
        return mkMapView
    }

    func updateUIView(_ view: MKMapView, context: Context) {

    }
}
