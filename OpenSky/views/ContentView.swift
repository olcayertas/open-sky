//
//  ContentView.swift
//  OpenSky
//
//  Created by Olcay Ertaş on 11.08.2020.
//  Copyright © 2020 Olcay Ertaş. All rights reserved.
//

import SwiftUI
import MapKit
import Combine
import os.log

struct ContentView: View {

    private let mapView = MapView()

    var body: some View {
        self.mapView.edgesIgnoringSafeArea(.all)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
