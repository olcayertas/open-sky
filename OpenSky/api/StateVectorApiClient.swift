//
// Created by Olcay Ertaş on 18.07.2020.
// Copyright (c) 2020 Olcay Ertaş. All rights reserved.
//

import Foundation
import Combine
import os.log


struct StateVectorApiClient: BaseApiClient {

    private class StateVectorResponse {
        var time: Int?
        var states: [[AnyObject]]?
    }

    private func parseResponse(data: Data) -> [StateVector] {
        var results: [StateVector] = []
        do {
            guard let response = try JSONSerialization.jsonObject(with: data) as? [String: Any] else {
                os.os_log("Failed to parse JSON!", log: .searchApiClient, type: .error)
                return results
            }
            guard let states = response["states"] as? [[Any]] else {
                os.os_log("Failed to get states!", log: .searchApiClient, type: .error)
                return results
            }
            states.forEach { vector in
                results.append(StateVector(data: vector))
            }
        } catch (let error) {
            os.os_log("Error: %{public}@", log: .searchApiClient, type: .error, error.localizedDescription)
        }
        return results
    }

    func getResults(
            lamin: Double,
            lamax: Double,
            lomin: Double,
            lomax: Double
    ) -> AnyPublisher<[StateVector], Error> {
        var components = self.components
        components.path = "/api/states/all"
        //TODO: Get user name and password from config file.
        components.queryItems = [
            URLQueryItem(name: "lamin", value: "\(lamin)"),
            URLQueryItem(name: "lamax", value: "\(lamax)"),
            URLQueryItem(name: "lomin", value: "\(lomin)"),
            URLQueryItem(name: "lomax", value: "\(lomax)")
        ]
        guard let url = components.url else {
            return Fail(error: ApiClientError.failedToBuildUrlFromComponents).eraseToAnyPublisher()
        }
        return fetch(url: url)
                .tryMap { (data, code) -> [StateVector] in
                    self.parseResponse(data: data)
                }
                .receive(on: RunLoop.main)
                .eraseToAnyPublisher()
    }
}

