//
// Created by Olcay Ertaş on 19.07.2020.
// Copyright (c) 2020 Olcay Ertaş. All rights reserved.
//

import Foundation
import Combine
import os.log

public enum ApiClientError: Error, LocalizedError {

    case notFound
    case badRequest
    case failedToBuildUrlFromComponents
    case failedToCreateUrl(String)
    case failedToDecodeResponse
    case networkError(URLError)
    case apiError(String)
    case serverError
    case unauthorized
    case unknown

    public var errorDescription: String? {
        switch self {
        case .badRequest:
            return "Bad request!"
        case .notFound:
            return "Not found!"
        case .unknown:
            return "Unknown error!"
        case .apiError(let error):
            return error
        case .failedToBuildUrlFromComponents:
            return "Did you set base URL?"
        case .failedToCreateUrl(let endpoint):
            return "Invalid URL: \(endpoint)"
        case .failedToDecodeResponse:
            return "Failed to decode response!"
        case .serverError:
            return "Server error!"
        case .networkError(let error):
            return error.localizedDescription
        case .unauthorized:
            return "Unauthorized!"
        }
    }
}

protocol BaseApiClient {
    var components: URLComponents { get }
    func fetch(url: URL) -> AnyPublisher<(Data, Int), Error>
}

extension BaseApiClient {

    internal var components: URLComponents {
        var components = URLComponents()
        components.scheme = "https"
        components.host = "opensky-network.org"
        components.user = "Ertas"
        components.password = "97qAy62NSWPBJNZ"
        return components
    }

    public func fetch(url: URL) -> AnyPublisher<(Data, Int), Error> {
        let request = URLRequest(url: url)
        os_log("%{public}@ %{public}@", log: .baseApiClient, type: .info, "\(request.httpMethod!)", "\(url.absoluteString)")
        let publisher = URLSession.shared.dataTaskPublisher(for: request)
        return publisher.tryMap { data, response -> (Data, Int) in
            guard let httpResponse = response as? HTTPURLResponse else {
                throw ApiClientError.serverError
            }
            os_log("%{public}@ %{public}@", log: .baseApiClient, type: .info, "\(httpResponse.statusCode)", url.absoluteString)
            return try self.handleStatusCode(
                    data: data,
                    statusCode: httpResponse.statusCode
            )
        }.eraseToAnyPublisher()
    }

    private func handleStatusCode(data: Data, statusCode: Int) throws -> (Data, Int) {
        switch statusCode {
        case 200:
            return (data, statusCode)
        case 201:
            return (data, statusCode)
        case 204:
            return (Data(), statusCode)
        case 400:
            throw ApiClientError.badRequest
        case 401:
            throw ApiClientError.unauthorized
        case 404:
            throw ApiClientError.notFound
        case 500:
            throw ApiClientError.serverError
        default:
            throw ApiClientError.unknown
        }
    }
}